/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto_gestor_db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Alumno
 */
public class conexion {
    private String url, nombre, clave;
    public conexion(){
        this.url="jdbc:mysql://localhost/";
        this.nombre="root";
        this.clave="";
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @param clave the clave to set
     */
    public void setClave(String clave) {
        this.clave = clave;
    }
    public Connection conectar(){
        Connection conec = null;
        try{
            conec = DriverManager.getConnection(url,nombre,clave);
        }
        catch(SQLException ex){
            System.out.println(ex);
        }
        return conec;
    }
}
